package ru.t1.aksenova.tm.service;

import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.marker.UnitCategory;
import ru.t1.aksenova.tm.migration.AbstractSchemeTest;

@Category(UnitCategory.class)
public final class ProjectServiceTest extends AbstractSchemeTest {
/*
    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final ILiquibaseService connectionService = new LiquibaseService(propertyService);

    @NotNull
    private static final ITaskDTOService taskService = new TaskDTOService(connectionService);

    @NotNull
    private static final IProjectDTOService projectService = new ProjectDTOService(connectionService);

    @NotNull
    private static final ISessionDTOService sessionService = new SessionDTOService(connectionService);

    @NotNull
    private static final IUserDTOService userService = new UserDTOService(propertyService, connectionService, projectService, taskService, sessionService);

    @NotNull
    private static String userId = "";

    @NotNull
    private static String adminId = "";

    public static void initConnectionService() throws Exception {
        final Liquibase liquibase = liquibase("changelog/changelog-master.xml");
        liquibase.dropAll();
        liquibase.update("scheme");
    }

    public static void clearData() {
        @Nullable UserDTO user = userService.findOneById(userId);
        if (user != null) userService.remove(user);
        user = userService.findOneById(adminId);
        if (user != null) userService.remove(user);
    }

    @BeforeClass
    public static void initData() throws Exception {
        initConnectionService();
        @NotNull final UserDTO user = userService.add(USER_TEST);
        userId = user.getId();
        @NotNull final UserDTO admin = userService.add(ADMIN_TEST);
        adminId = admin.getId();
        USER_PROJECT1.setUserId(userId);
        USER_PROJECT2.setUserId(userId);
        ADMIN_PROJECT1.setUserId(adminId);
        ADMIN_PROJECT2.setUserId(adminId);
    }

    @AfterClass
    public static void destroy() {
        clearData();
        connectionService.close();
    }

    @Before
    public void beforeTest() {
        projectService.add(USER_PROJECT1);
        projectService.add(USER_PROJECT2);
    }

    @After
    public void afterTest() {
        projectService.removeAll(userId);
        projectService.removeAll(adminId);
    }

    @Test
    public void add() {
        Assert.assertThrows(Exception.class, () -> projectService.add(NULL_PROJECT));
        Assert.assertNotNull(projectService.add(ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = projectService.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void addByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.add(null, ADMIN_PROJECT1));
        Assert.assertThrows(Exception.class, () -> projectService.add("", ADMIN_PROJECT1));
        Assert.assertNotNull(projectService.add(adminId, ADMIN_PROJECT1));
        @Nullable final ProjectDTO project = projectService.findOneById(adminId, ADMIN_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getId(), project.getId());
        Assert.assertEquals(ADMIN_PROJECT1.getUserId(), project.getUserId());
        Assert.assertEquals(ADMIN_PROJECT1.getStatus(), project.getStatus());
    }

    @Test
    public void createByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.create(null, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.create(adminId, null, ADMIN_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.create(adminId, ADMIN_PROJECT1.getName(), null));
        @NotNull final ProjectDTO project = projectService.create(adminId, ADMIN_PROJECT1.getName(), ADMIN_PROJECT1.getDescription());
        Assert.assertEquals(ADMIN_PROJECT1.getName(), project.getName());
        Assert.assertEquals(ADMIN_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(adminId, project.getUserId());
    }

    @Test
    public void updateByUserIdById() {
        Assert.assertThrows(Exception.class, () -> projectService.updateById(null, USER_PROJECT1.getId(), USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.updateById(userId, null, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.updateById(userId, NON_EXISTING_PROJECT_ID, USER_PROJECT1.getName(), USER_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.updateById(userId, USER_PROJECT1.getId(), null, USER_PROJECT1.getDescription()));
        Assert.assertThrows(Exception.class, () -> projectService.updateById(userId, USER_PROJECT1.getId(), USER_PROJECT1.getName(), null));
        @NotNull final ProjectDTO project = projectService.updateById(userId, USER_PROJECT1.getId(), PROJECT_NAME, PROJECT_DESCR);
        Assert.assertEquals(PROJECT_NAME, project.getName());
        Assert.assertEquals(PROJECT_DESCR, project.getDescription());
        Assert.assertEquals(userId, project.getUserId());
    }

    private int getIndexFromList(@NotNull final List<ProjectDTO> projects, @NotNull final String id) {
        int index = 0;
        for (ProjectDTO project : projects) {
            index++;
            if (id.equals(project.getId())) return index - 1;
        }
        return -1;
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(Exception.class, () -> projectService.changeProjectStatusById(null, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        Assert.assertThrows(Exception.class, () -> projectService.changeProjectStatusById(userId, null, Status.IN_PROGRESS));
        Assert.assertThrows(Exception.class, () -> projectService.changeProjectStatusById(userId, USER_PROJECT1.getId(), null));
        Assert.assertThrows(Exception.class, () -> projectService.changeProjectStatusById(userId, NON_EXISTING_PROJECT_ID, Status.IN_PROGRESS));
        Assert.assertThrows(Exception.class, () -> projectService.changeProjectStatusById(NON_EXISTING_USER_ID, USER_PROJECT1.getId(), Status.IN_PROGRESS));
        @NotNull final ProjectDTO project = projectService.changeProjectStatusById(userId, USER_PROJECT1.getId(), Status.IN_PROGRESS);
        @NotNull final ProjectDTO project2 = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertEquals(project2.getId(), project.getId());
        Assert.assertEquals(project2.getStatus(), project.getStatus());
        Assert.assertEquals(project2.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void findAllByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.findAll(""));
        Assert.assertEquals(Collections.emptyList(), projectService.findAll(NON_EXISTING_USER_ID));
        final List<ProjectDTO> projects = projectService.findAll(userId);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
    }

    @Test
    public void findAllComparator() {
        projectService.removeAll(userId);
        projectService.set(USER_PROJECT_LIST);
        projectService.set(ADMIN_PROJECT_LIST);
        @NotNull final Comparator comparator = NameComparator.INSTANCE;
        final List<ProjectDTO> projects = projectService.findAll(userId, comparator);
        projects.forEach(project -> Assert.assertEquals(userId, project.getUserId()));
        final List<ProjectDTO> projects2 = projectService.findAll(adminId, comparator);
        projects2.forEach(project -> Assert.assertEquals(adminId, project.getUserId()));
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void findOneByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.findOneById("", USER_PROJECT1.getId()));
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(userId, null));
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.findOneById(userId, USER_PROJECT1.getId());
        Assert.assertNotNull(project);
        Assert.assertEquals(USER_PROJECT1.getName(), project.getName());
        Assert.assertEquals(USER_PROJECT1.getDescription(), project.getDescription());
        Assert.assertEquals(USER_PROJECT1.getUserId(), project.getUserId());
    }

    @Test
    public void existsById() {
        Assert.assertFalse(projectService.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.existsById(userId, null));
        Assert.assertFalse(projectService.existsById(userId, NON_EXISTING_PROJECT_ID));
        Assert.assertTrue(projectService.existsById(userId, USER_PROJECT1.getId()));
    }

    @Test
    public void removeAll() {
        projectService.removeAll(userId);
        projectService.removeAll(adminId);
        Assert.assertEquals(0, projectService.getSize(userId));
        Assert.assertEquals(0, projectService.getSize(adminId));
        projectService.set(PROJECT_LIST);
        Assert.assertNotEquals(0, projectService.getSize(userId));
        Assert.assertNotEquals(0, projectService.getSize(adminId));
    }

    @Test
    public void removeOne() {
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT1);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT1.getId()));
        projectService.remove(adminId, project);
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT1.getId()));
    }

    @Test
    public void removeOneById() {
        Assert.assertThrows(Exception.class, () -> projectService.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT2);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
        projectService.removeOneById(adminId, project.getId());
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertThrows(Exception.class, () -> projectService.removeOneById(null, ADMIN_PROJECT2.getId()));
        Assert.assertThrows(Exception.class, () -> projectService.removeOneById(adminId, null));
        Assert.assertThrows(Exception.class, () -> projectService.removeOneById(userId, NON_EXISTING_PROJECT_ID));
        @Nullable final ProjectDTO project = projectService.add(ADMIN_PROJECT2);
        Assert.assertNotNull(projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
        projectService.removeOneById(adminId, project.getId());
        Assert.assertThrows(Exception.class, () -> projectService.findOneById(adminId, ADMIN_PROJECT2.getId()));
    }
*/
}
