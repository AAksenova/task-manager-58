package ru.t1.aksenova.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint;
import ru.t1.aksenova.tm.api.service.IAdminService;
import ru.t1.aksenova.tm.dto.request.DropSchemeRequest;
import ru.t1.aksenova.tm.dto.request.InitSchemeRequest;
import ru.t1.aksenova.tm.dto.response.DropSchemeResponse;
import ru.t1.aksenova.tm.dto.response.InitSchemeResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@Controller
@AllArgsConstructor
@WebService(endpointInterface = "ru.t1.aksenova.tm.api.endpoint.IAdminEndpoint")
public final class AdminEndpoint extends AbstractEndpoint implements IAdminEndpoint {

    @Override
    @WebMethod
    @SneakyThrows
    public DropSchemeResponse dropDBScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final DropSchemeRequest request) {
        @Nullable final String token = request.getToken();
        @Nullable final IAdminService adminService = getServiceLocator().getAdminService();
        @Nullable String result = adminService.getDropScheme(token);
        return new DropSchemeResponse(result);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public InitSchemeResponse initDBScheme(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final InitSchemeRequest request) {
        @Nullable final String token = request.getToken();
        @Nullable final IAdminService adminService = getServiceLocator().getAdminService();
        @Nullable String result = adminService.getInitScheme(token);
        return new InitSchemeResponse(result);
    }

}
