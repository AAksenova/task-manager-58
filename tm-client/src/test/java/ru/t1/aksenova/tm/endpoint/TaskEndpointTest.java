package ru.t1.aksenova.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.aksenova.tm.api.endpoint.IAuthEndpoint;
import ru.t1.aksenova.tm.api.endpoint.IProjectEndpoint;
import ru.t1.aksenova.tm.api.endpoint.ITaskEndpoint;
import ru.t1.aksenova.tm.api.service.IPropertyService;
import ru.t1.aksenova.tm.dto.model.ProjectDTO;
import ru.t1.aksenova.tm.dto.model.TaskDTO;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.marker.IntegrationCategory;
import ru.t1.aksenova.tm.service.PropertyService;

import static ru.t1.aksenova.tm.constant.ProjectTestData.*;
import static ru.t1.aksenova.tm.constant.TaskTestData.*;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_LOGIN;
import static ru.t1.aksenova.tm.constant.UserTestData.ADMIN_PASSWORD;

@Ignore
@Category(IntegrationCategory.class)
public class TaskEndpointTest {

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IAuthEndpoint authEndpointClient = IAuthEndpoint.newInstance(propertyService);

    @NotNull
    private static final ITaskEndpoint taskEndpointClient = ITaskEndpoint.newInstance(propertyService);

    @NotNull
    private static final IProjectEndpoint projectEndpointClient = IProjectEndpoint.newInstance(propertyService);

    @Nullable
    private static String token;

    @Nullable
    private static ProjectDTO projectOne = new ProjectDTO();

    @Nullable
    private static TaskDTO taskAlfa = new TaskDTO();

    @Nullable
    private static TaskDTO taskBetta = new TaskDTO();

    private static int taskAlfaIndex;

    private static int taskBettaIndex;

    @BeforeClass
    public static void setUp() {
        @NotNull final UserLoginRequest loginRequest = new UserLoginRequest();
        loginRequest.setLogin(ADMIN_LOGIN);
        loginRequest.setPassword(ADMIN_PASSWORD);
        token = authEndpointClient.login(loginRequest).getToken();

        projectOne = createTestProject(ONE_PROJECT_NAME, ONE_PROJECT_DESCRIPTION);

        taskAlfa = createTestTask(ALFA_TASK_NAME, ALFA_TASK_DESCRIPTION);
        taskAlfaIndex = 0;
        bindToProject(projectOne.getId(), taskAlfa.getId());

        taskBetta = createTestTask(BETTA_TASK_NAME, BETTA_TASK_DESCRIPTION);
        taskBettaIndex = 1;
        bindToProject(projectOne.getId(), taskBetta.getId());
    }

    public static TaskDTO createTestTask(@NotNull final String name, @NotNull final String description) {
        @NotNull TaskCreateRequest requestCreate = new TaskCreateRequest(token);
        requestCreate.setName(name);
        requestCreate.setDescription(description);
        return taskEndpointClient.createTask(requestCreate).getTask();
    }

    public static ProjectDTO createTestProject(@NotNull final String name, @NotNull final String description) {
        @NotNull ProjectCreateRequest requestCreate = new ProjectCreateRequest(token);
        requestCreate.setName(name);
        requestCreate.setDescription(description);
        return projectEndpointClient.createProject(requestCreate).getProject();
    }

    public static void bindToProject(@NotNull final String projectId, @NotNull final String taskId) {
        @NotNull TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(token);
        requestBind.setTaskId(taskId);
        requestBind.setProjectId(projectId);
        taskEndpointClient.bindTaskToProject(requestBind);
    }

    @AfterClass
    public static void tearDown() {
        @NotNull TaskClearRequest requestTaskClear = new TaskClearRequest(token);
        taskEndpointClient.clearTask(requestTaskClear);

        @NotNull ProjectClearRequest requestProjectClear = new ProjectClearRequest(token);
        projectEndpointClient.clearProject(requestProjectClear);

        @NotNull final UserLogoutRequest requestLogout = new UserLogoutRequest(token);
        authEndpointClient.logout(requestLogout);
    }

    @Test
    public void bindUnbaindTaskToProject() {
        @NotNull TaskUnbindFromProjectRequest requestUnbind = new TaskUnbindFromProjectRequest(token);
        requestUnbind.setTaskId(taskAlfa.getId());
        requestUnbind.setProjectId(projectOne.getId());
        Assert.assertNotNull(taskEndpointClient.unbindTaskFromProject(requestUnbind));

        @NotNull TaskBindToProjectRequest requestBind = new TaskBindToProjectRequest(token);
        requestBind.setTaskId(taskAlfa.getId());
        requestBind.setProjectId(projectOne.getId());
        Assert.assertNotNull(taskEndpointClient.bindTaskToProject(requestBind));
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertNotNull(token);
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(token);
        request.setId(taskAlfa.getId());
        request.setStatus(STATUS_IN_PROGRESS);
        Assert.assertNotNull(taskEndpointClient.changeTaskStatusById(request));
    }


    @Test
    public void clearTask() {
        Assert.assertNotNull(token);
        @NotNull final TaskClearRequest request = new TaskClearRequest(token);
        Assert.assertNotNull(taskEndpointClient.clearTask(request));
        taskAlfa = createTestTask(ALFA_TASK_NAME, ALFA_TASK_DESCRIPTION);
        taskAlfaIndex = 0;
        bindToProject(projectOne.getId(), taskAlfa.getId());

        taskBetta = createTestTask(BETTA_TASK_NAME, BETTA_TASK_DESCRIPTION);
        taskBettaIndex = 1;
        bindToProject(projectOne.getId(), taskBetta.getId());
    }

    @Test
    public void completeTaskById() {
        Assert.assertNotNull(token);
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(token);
        request.setId(taskAlfa.getId());
        Assert.assertNotNull(taskEndpointClient.completeTaskById(request));
    }

    @Test
    public void createTask() {
        @NotNull TaskCreateRequest requestCreate = new TaskCreateRequest(token);
        requestCreate.setName(GAMMA_TASK_NAME);
        requestCreate.setDescription(GAMMA_TASK_DESCRIPTION);
        Assert.assertNotNull(taskEndpointClient.createTask(requestCreate).getTask());
    }

    @Ignore
    @Test
    public void listTask() {
        Assert.assertNotNull(token);
        @NotNull final TaskListRequest request = new TaskListRequest(token);
        request.setSort("BY_CREATED");
        Assert.assertNotNull(taskEndpointClient.listTask(request));
    }

    @Test
    public void listTaskByProjectId() {
        Assert.assertNotNull(token);
        @NotNull final TaskListByProjectIdRequest request = new TaskListByProjectIdRequest(token);
        request.setProjectId(projectOne.getId());
        Assert.assertNotNull(taskEndpointClient.listTaskByProjectId(request));
    }

    @Test
    public void removeTaskById() {
        Assert.assertNotNull(token);
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(token);
        request.setId(taskBetta.getId());
        Assert.assertNotNull(taskEndpointClient.removeTaskById(request));

        taskBetta = createTestTask(BETTA_TASK_NAME, BETTA_TASK_DESCRIPTION);
        taskBettaIndex = 1;
        bindToProject(projectOne.getId(), taskBetta.getId());
    }

    @Test
    public void showTaskById() {
        Assert.assertNotNull(token);
        @NotNull final TaskShowByIdRequest request = new TaskShowByIdRequest(token);
        request.setId(taskAlfa.getId());
        Assert.assertNotNull(taskEndpointClient.showTaskById(request));
    }

    @Test
    public void startTaskById() {
        Assert.assertNotNull(token);
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(token);
        request.setId(taskAlfa.getId());
        Assert.assertNotNull(taskEndpointClient.startTaskById(request));
    }


    @Test
    public void updateTaskById() {
        Assert.assertNotNull(token);
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(token);
        request.setId(taskAlfa.getId());
        request.setName(ALFA_TASK_NAME + " update");
        request.setDescription(ALFA_TASK_DESCRIPTION + " update");
        Assert.assertNotNull(taskEndpointClient.updateTaskById(request));
    }

}
