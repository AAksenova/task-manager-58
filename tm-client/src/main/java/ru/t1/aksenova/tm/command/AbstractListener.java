package ru.t1.aksenova.tm.command;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.api.model.ICommand;
import ru.t1.aksenova.tm.api.service.IServiceLocator;
import ru.t1.aksenova.tm.event.ConsoleEvent;

@Getter
@Setter
@Component
public abstract class AbstractListener implements ICommand {

    @Nullable
    @Autowired
    protected IServiceLocator serviceLocator;

    @Nullable
    public abstract String getArgument();

    @Nullable
    public abstract String getDescription();

    @Nullable
    public abstract String getName();

    public abstract void handler(@NotNull final ConsoleEvent consoleEvent);

    @Nullable
    protected String getToken() {
        return getServiceLocator().getTokenService().getToken();
    }

    protected void setToken(@Nullable final String token) {
        getServiceLocator().getTokenService().setToken(token);

    }

    @NotNull
    public String toString() {
        @Nullable final String name = getName();
        @Nullable final String argument = getArgument();
        @Nullable final String description = getDescription();
        String result = "";
        if (name != null && !name.isEmpty()) result += name;
        if (argument != null && !argument.isEmpty()) result += ", " + argument;
        if (description != null && !description.isEmpty()) result += ": " + description;
        return result;

    }

}
