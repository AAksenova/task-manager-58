package ru.t1.aksenova.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.aksenova.tm.dto.model.UserDTO;
import ru.t1.aksenova.tm.dto.request.UserRegistryRequest;
import ru.t1.aksenova.tm.dto.response.UserRegistryResponse;
import ru.t1.aksenova.tm.enumerated.Role;
import ru.t1.aksenova.tm.event.ConsoleEvent;
import ru.t1.aksenova.tm.util.TerminalUtil;

@Component
public final class UserRegistryListener extends AbstractUserListener {

    @NotNull
    public static final String NAME = "user-registry";

    @NotNull
    public static final String DESCRIPTION = "User registration.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userRegistryListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        @NotNull final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        @NotNull final String email = TerminalUtil.nextLine();

        @Nullable final UserRegistryRequest request = new UserRegistryRequest(getToken());
        request.setLogin(login);
        request.setPassword(password);
        request.setEmail(email);
        @Nullable final UserRegistryResponse response = getUserEndpoint().registryUser(request);
        @Nullable final UserDTO user = response.getUser();
        showUser(user);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
