package ru.t1.aksenova.tm.api.endpoint;


public interface IConnectionProvider {

    String getHost();

    String getPort();

}
